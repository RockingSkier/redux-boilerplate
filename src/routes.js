import React from 'react';

import { Route } from 'react-router';

import App from './containers/App.jsx';
import DevTools from './containers/DevTools.jsx';
import Home from './containers/Home.jsx';

const routes = (
  <Route component={App}>
    <Route component={Home} path="/" />
  </Route>
);

export default routes;
