import React from 'react';

import { Component } from 'react';

import Spinner from '../components/Spinner.jsx';


export default class LoadingContainer extends Component {
  render() {
    return (
      <div>
        <h1>The applications is Loading</h1>
        <Spinner />
      </div>
    );
  }
}
