import React from 'react';

import { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';


function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {};
}

@connect(
  mapStateToProps,
  mapDispatchToProps
)
export default class AppContainer extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
  }

  render() {
    const {
      children,
    } = this.props;

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12">
            <Helmet titleTemplate="%s | Redux Boilerplate" />
            {children}
          </div>
        </div>
      </div>
    );
  }
}
