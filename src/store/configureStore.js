import { createStore, applyMiddleware, compose } from 'redux';
import { reduxReactRouter } from 'redux-router';
import createHistory from 'history/lib/createBrowserHistory';
import thunk from 'redux-thunk';
import promise from 'redux-promise';

import DevTools from '../containers/DevTools.jsx';
import rootReducer from '../reducers/index.js';
import routes from '../routes.js';


const finalCreateStore = compose(
  applyMiddleware(promise),
  applyMiddleware(thunk),
  reduxReactRouter({ routes, createHistory }),
  DevTools.instrument(),
)(createStore);


export default function configureStore(initialState) {
  return finalCreateStore(rootReducer, initialState);
}
