import React from 'react';

import { Provider } from 'react-redux';
import { ReduxRouter } from 'redux-router';
import { Route, Router } from 'react-router';
import ReactDOM from 'react-dom';

import configureStore from './store/configureStore.js';
import DevTools from './containers/DevTools.jsx';


const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <div>
      <ReduxRouter>
      </ReduxRouter>
      {DEBUG ? <DevTools /> : null}
    </div>
  </Provider>,
  document.getElementById('root')
);
